{
	'name' : 'Website EdRP Oohel',
	'summary': 'Sitio web EDRP',
	'description': """
		Portal web EdRP para presentación de poryectos derrollados, parents y descripción de módulos.
	""",
    'category': 'Website',
	# 'category': 'Theme/Creative',
    'version': '0.1',
    'depends': ['website'],
    
	"author": 'Oohel Technologies S.A de C.V',
    "website": "http://www.oohel.net/edrp",
    'data': [
        'views/assets.xml',
        'views/view_index.xml',
    ],
    'application': True,
    'auto_install': False,
    'installable': True,

}